Congratulations! You've made it to the project stage of our interview process.

Please fork this project and complete the challenges you'll find under the code_review and pattern_matching directories. When you've finished, please share your project with @forrestblount with access so that I can share with additional team members as needed.

If you have any questions or require more time to complete these challenges, don't hesitate to reach out.
